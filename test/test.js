var supertest = require("supertest");
var mocha = require('mocha');
var reqData=require('../reqData.json');
var chai = require('chai');
const expect = require('chai').expect;
chai.should();

var server = supertest.agent(reqData.host);
var mocha = new mocha({reporter: 'mochawesome'});
describe(reqData.heading, function(){  
    reqData.TC_Obj.forEach(function(TC_Obj){
        it(TC_Obj.title,function(done){
            server
            .post(TC_Obj.path)
            .send(TC_Obj.body)
            .set(TC_Obj.header)
            .expect(function(res){
                const resObj = JSON.parse(res.text);
                if (resObj.hasOwnProperty('message')) {
                    const re="unsuccess";
                    expect(re).to.equal(TC_Obj.response);
                }
                else
                {
                    expect(resObj.dataObj.status).to.equal(TC_Obj.response); 
                }
            })
        .end(done)
        });
    })       
});