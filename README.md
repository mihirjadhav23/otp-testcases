# Generate OTP Testcases

This project will test generate OTP testcases and generate report on that as well.

## Deploy
1. Run below command to install required modules.
```node.JS
npm i
```
2. Save and run below command to execute testcase.
```node.JS
npm run test
```
3. Save and run below command to generate testcase report.
```node.JS
npm run report
```
# 
You will get generated report in below directory 

```
mochawesome-report\mochawesome.html
```